﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Lab5.Test.Fakes
{
    class DependingFakeImpl : IDependingFake
    {
        IFake fake;

        public DependingFakeImpl(IFake fake)
        {
            this.fake = fake;
        }

        public IFake Fake
        {
            get { return fake; }

            set { this.fake = value; }
        }
    }
}
